Source: ruby-sawyer
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               ruby-addressable (>= 2.3.5),
               ruby-faraday (>= 0.8~)
Standards-Version: 4.4.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-sawyer
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-sawyer
Homepage: https://github.com/lostisland/sawyer
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-sawyer
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-addressable (>= 2.3.5),
         ruby-faraday (>= 0.8~),
         ${misc:Depends},
         ${shlibs:Depends}
Description: HTTP/REST API client Ruby library
 Sawyer is an experimental hypermedia HTTP client built on top of ruby-faraday.
 .
 Although Sawyer provides the expected features of an HTTP client library,
 it's more useful when is used to interact and to implement REST APIs
 endpoints.
 .
 Out of the box it can parse data from API responses, turns contents into
 high level resources or collections of resources, relations between those
 resources, etc.
